// Disables copy/paste functions on Email field.

window.onload = function() {
 const myInput = document.getElementById('myInput');
 myInput.onpaste = function(e) {
   e.preventDefault();
 }
}
