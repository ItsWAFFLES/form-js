// Disables copy/paste functions on Email field.

<script type="text/javascript">
$(document).ready(function() {
    $('#email').bind("cut copy paste", function(e) {
        e.preventDefault();
        alert("You cannot paste text into this textbox!");
        $('#email').bind("contextmenu", function(e) {
            e.preventDefault();
        });
    });
});
</script>